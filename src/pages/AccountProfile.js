import { Form, Button, Nav } from 'react-bootstrap';
import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';
import UserContext from '../UserContext';
import { swalSuccess } from '../utils';
import { swalError } from '../utils';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import fetcher from '../fetcher';
import { Link } from 'react-router-dom';
import EditField from '../components/EditField';
import { useNavigate } from 'react-router-dom';
import OrderGallery from '../components/OrderGallery';
import AccountPassword from './AccountPassword';

export default function AccountProfile({ userId, admin }) {
  const [user, setUser] = useState(null);
  const { user: currentUser } = useContext(UserContext);

  const navigate = useNavigate();


  const [email, setEmail] = useState();
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [mobileNo, setMobileNo] = useState();
  const [address, setAddress] = useState();
  const [isAdmin, setIsAdmin] = useState();
  //const userid = user._id;

  useEffect(() => {
    fetcher.user(userId)
      .then(({ status, data }) => {
        if (status === 200) {
          setEmail(data.email);
          setFirstName(data.firstName);
          setLastName(data.lastName);
          setMobileNo(data.mobileNo);
          setAddress(data.address);
          setIsAdmin(data.isAdmin);
        }
      })
  }, []);

  function sendUpdate(field) {
    fetcher.updateUser(userId, field).then(({ status, data }) => {
      if (status === 200)
        swalSuccess('Successfully updated');
      else
        swalError('Cannot update user');
    });
  }

  function updateFirstName(value) { sendUpdate({ firstName: value }, setFirstName(value))}
  function updateLastName(value) { sendUpdate({ lastName: value }, setLastName(value))}
  function updateEmail(value) { sendUpdate({ email: value }, setEmail(value))}
  function updateMobileNo(value) { sendUpdate({ mobileNo: value }, setMobileNo(value))}
  function updateAddress(value) { sendUpdate({ address: value }, setAddress(value))}
  function updateIsAdmin(value) { sendUpdate({ isAdmin: value }, setIsAdmin(value))}


  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetcher.userOrders(userId)
      .then(({status, data}) => {
        if (status === 200) {
          setOrders(data.reverse())
        }
      })
  }, []);

  return (
    <Row>
      <Col md={{ span: 6, offset: 3 }}>
        <ul className="editable-list list-unstyled">
          {currentUser.isAdmin && <li><Button onClick={() => { navigate(-1) } }>Back</Button></li>}
          <li>
            <EditField value={firstName} label={"First name"} onConfirm={updateFirstName} />
          </li>
    
          <li>
            <EditField value={lastName} label={"Last name"} onConfirm={updateLastName} />
          </li>
          <li>
            <EditField value={email} label={"Email"} onConfirm={updateEmail} />
          </li>
          <li>
            <EditField value={mobileNo} label={"Mobile"} onConfirm={updateMobileNo} />
          </li>
          <li>
            <EditField value={address} label={"Address"} onConfirm={updateAddress} />
          </li>
    {admin ?
      <>
      <li>
      <EditField value={isAdmin} label={"IsAdmin"} onConfirm={updateIsAdmin} />
      </li>
      </>
      :
      <AccountPassword />
    }
        </ul>
      </Col>
    {admin &&
      <OrderGallery orders={orders} admin={true} />
    }
    </Row>
  );
}

// vim: set ft=javascriptreact
