import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import fetcher from '../fetcher';
import { swalError } from '../utils';
import { swalSuccess } from '../utils';
import { Link } from 'react-router-dom';

export default function Register() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [address, setAddress] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');

  const [isActive, setIsActive] = useState(false);

  function registerUser(e) {
    e.preventDefault();

    fetcher.checkEmail(email)
      .then(({ data }) => {
        if (data === true)
          swalError('Duplicate email found')
        else {
          const userData = {
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: mobileNo,
              address: address,
              password: password1,
            }

          fetcher.createUser(userData)
            .then(({data}) => {
              if (data) {
                setFirstName('');
                setLastName('');
                setEmail('');
                setAddress('');
                setMobileNo('');
                setPassword1('');
                setPassword2('');
                swalSuccess('Registration successful')
                navigate('/login');
              }
            })
        }
      })
  }

  /*

        Two Way Binding

        -it is done so that we can assure that we can save the input into our states as we type into the input elements. This is done so what we don't have to save it just before we submit

        e.target - current element where the event happens

        e.target.value - current value of the element where the event happened

    */

  useEffect(() => {
    // Enable the submit button if:

    // All the fields are populated.

    // both passwords match.

    if (
      firstName !== '' &&
      lastName !== '' &&
      mobileNo.length !== '' &&
      email !== '' &&
      address !== '' &&
      password1 !== '' &&
      password2 !== '' &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNo, password1, password2, address]);

  return (user) ? (
    <Navigate to="/"></Navigate>
  ) : (
    <Container>
      <Row>
        <Col md={{ span: 6, offset: 3 }} className="border rounded py-3">
          <h1>Register</h1>
          <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group className="mb-3" controlId="firstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="First name"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="lastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Last name"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="mobileNo">
              <Form.Label>Mobile number</Form.Label>
              <Form.Control
                type="text"
                placeholder="09123456789"
                value={mobileNo}
                onChange={(e) => setMobileNo(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="emailAddress">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="address">
              <Form.Label>Address</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter address"
                value={address}
                onChange={(e) => setAddress(e.target.value)}
                required
              />
              <Form.Text className="text-muted">
                Items will be delivered here.
              </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password1">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password1}
                onChange={(e) => setPassword1(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password2">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Verify password"
                value={password2}
                onChange={(e) => setPassword2(e.target.value)}
                required
              />
            </Form.Group>

            {isActive ? (
              <Button variant="success" type="submit" id="submitBtn">
                Submit
              </Button>
            ) : (
              <Button variant="light" type="submit" id="submitBtn" disabled>
                Submit
              </Button>
            )}
          </Form>
            <div className="mt-3">Already have an account? <Link to={"/login"}>Log in here</Link></div>
        </Col>
      </Row>
    </Container>
  );
}

// vim: ft=javascriptreact
