import { Navigate } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Logout() {
  const { unsetUser } = useContext(UserContext);

  localStorage.clear();
  unsetUser();

  Swal.fire({
    title: 'Log out successful',
    icon: 'success',
    toast: true,
    timerProgressBar: true,
    timer: 2000,
    position: 'top'
  });

  return (
    <Navigate to="/" />
  )
};

// vim: ft=javascriptreact
