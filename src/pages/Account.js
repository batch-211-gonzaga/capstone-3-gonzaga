import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Nav } from 'react-bootstrap';

import UserContext from '../UserContext';

import AccountProfile from '../pages/AccountProfile';
import AccountOrders from '../pages/AccountOrders';
import PageNotFound from './PageNotFound';
import LogInPrompt from '../components/LogInPrompt';
import Cart from './Cart';
import Wishlist from './Wishlist';

export default function Account() {
  const { section } = useParams();

  const navigate = useNavigate();

  const { user } = useContext(UserContext);
  const [activeCart, setActiveCart] = useState('');
  const [activeWishlist, setActiveWishlist] = useState('');
  const [activeOrders, setActiveOrders] = useState('');
  const [activeProfile, setActiveProfile] = useState('');

  function unsetSelection() {
      setActiveCart('');
      setActiveWishlist('');
      setActiveOrders('');
      setActiveProfile('');
  }

  function selectSection(sectionArg) {
    unsetSelection();

    if (sectionArg === 'cart') {
      setActiveCart('active');
      navigate('/account/cart');
    }
    if (sectionArg === 'wishlist') {
      setActiveWishlist('active');
      navigate('/account/wishlist');
    }
    if (sectionArg === 'orders') {
      setActiveOrders('active');
      navigate('/account/orders');
    }
    if (sectionArg === 'profile') {
      setActiveProfile('active');
      navigate('/account/profile');
    }
  }

  function showPageNotFound() {
    if (
      !section === 'cart' ||
      !section === 'wishlist' ||
      !section === 'orders' ||
      !section === 'profile'
    )

    return <PageNotFound />;
  }

  useEffect(() => {
    setActiveCart('');
    setActiveWishlist('');
    setActiveOrders('');
    setActiveProfile('');

    if (section === undefined) {
      selectSection('cart')
    } else
      selectSection(section);
  }, [section]);

  let toRender = <></>;
  if (!user)
    toRender = <LogInPrompt returnUrl={window.location.pathname} />;
  else if (user && user.isAdmin)
    toRender = <h1>This page is for non-admin users only</h1>
  else
    toRender = (
      <>
        <Nav className="justify-content-center">
          <h2 className="justify-content-center">Account dashboard</h2>
        </Nav>
        <Nav justify variant="tabs" className="justify-content-center">
          <Nav.Item>
            <Nav.Link
              eventKey="cart"
              className={activeCart}
              onClick={(e) => selectSection('cart')}
            >
              Cart
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link
              eventKey="wishlist"
              className={activeWishlist}
              onClick={(e) => selectSection('wishlist')}
            >
              Wishlist
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link
              eventKey="orders"
              className={activeOrders}
              onClick={(e) => selectSection('orders')}
            >
              Orders
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link
              eventKey="profile"
              className={activeProfile}
              onClick={(e) => selectSection('profile')}
            >
              Profile
            </Nav.Link>
          </Nav.Item>
        </Nav>
        <Container className="border border-top-0 p-3">
            {activeCart && <Cart />}
            {activeWishlist && <Wishlist />}
            {activeOrders && <AccountOrders />}
            {activeProfile && <AccountProfile userId={user._id} />}
            {showPageNotFound()}
        </Container>
      </>
    );

  return toRender;
};

// vim: ft=javascriptreact
