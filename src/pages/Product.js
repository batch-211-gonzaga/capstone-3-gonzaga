import { useEffect, useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { Container, Badge, Button, Row, Col, Image } from 'react-bootstrap';
import Stars from '../components/Stars';
import fetcher from '../fetcher';
import ProductActions from '../components/ProductActions';
import ProductReviews from '../components/ProductReviews';

export default function Product() {

  const [product, setProduct] = useState({});
  const [productId, setProductId] = useState('');

  const { slug } = useParams();
  const returnUrl = encodeURIComponent(`product/${slug}`);

  useEffect(() => {
    fetcher.productFromSlug(slug)
    .then(({status, data}) => {
      setProduct(data);
    })
  }, [])

  return !product.name ? (<></>) : (
    <Container className="product-details">
      <Row className="my-5">
        <Col md={12} lg={4} className="mb-3 center-children">
          <div className="centered-child"><Image src={product.imageUrl} /></div>
        </Col>
        <Col md={12} lg={6} className="center-children">
          <div className="centered-child left-children v-centered-child">
            <h2>{product.name}</h2>

            {(!product.isActive) &&
              <Badge bg="danger" className="my-2">Not available</Badge>
            }

            <h3>&#8369; {product.price.toLocaleString('en-PH')} </h3>
            {
              (product.ratings.length === 0) ? <></> :
                <h3><Stars rating={product.rating} /> ({product.rating.toFixed(2)}) {product.ratings.length} ratings</h3>
            }
            {
              (product.numSold === 0) ? <></> :
                <h3>{product.numSold} sold</h3>
            }
          <ProductActions returnUrl={returnUrl} product={product} />
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          <span>{product.description}</span>
        </Col>
      </Row>
      {product && <ProductReviews ratings={product.ratings}/>}
    </Container>
  )
};
