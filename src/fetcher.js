//const BASE_URL =  process.env.BACKEND_URL || 'http://localhost:4000';
//const BASE_URL = 'https://capstone-2-gonzaga.onrender.com'
//const FRONTEND_BASE_URL = 'https://capstone-3-gonzaga.vercel.app'
const BASE_URL = 'https://ecomapi.gerrygonzaga.com';
const FRONTEND_BASE_URL = 'https://ecom.gerrygonzaga.com';

const fetcher = {};

async function jsonStatusBody(res) {
  return { status: res.status, data: await res.json() }
}

const getAuthHeader = () => {
  const token = localStorage.getItem('token');
  return `Bearer ${token}`;
}

function runFetch(endPoint, method, { sendToken, body }) {
  const options = { method: method, headers: { 'Content-Type': 'application/json' } };
  if (sendToken)
    options.headers.Authorization = getAuthHeader();

  if (body)
    options.body = JSON.stringify(body);

  return fetch(`${BASE_URL}${endPoint}`, options)
    .then(async res => await jsonStatusBody(res));
};

// /users
fetcher.checkEmail = (email) => {
  const body = { email: email };
  return runFetch('/users/checkEmail', 'POST', { body: body })
}

fetcher.createUser = (userData) => {
  return runFetch('/users', 'POST', { body: userData });
}

fetcher.user = (userId) => {
  return fetch(`${BASE_URL}/users/${userId}`, {
    method: 'GET',
    headers: { Authorization: getAuthHeader() }
  })
  .then(res => jsonStatusBody(res));
};

fetcher.users = () => {
  return runFetch('/users', 'GET', { sendToken: true });
};

fetcher.updateUser = (userId, userData) => {
  return fetch(`${BASE_URL}/users/${userId}`, {
    method: 'PATCH',
    headers: { Authorization: getAuthHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(userData),
  })
  .then(res => jsonStatusBody(res));
};

fetcher.login = (email, password) => {
  const body = { email: email, password: password };
  return runFetch('/users/login', 'POST', { body: body })
};

// /products
fetcher.product = async (productId) => {
  return fetch(`${BASE_URL}/products/${productId}`, {
    method: 'GET'
  })
  .then(res => jsonStatusBody(res));
};

fetcher.productFromSlug = (slug) => {
  return fetch(`${BASE_URL}/products/slug/${slug}`, {
    method: 'GET'
  })
  .then(res => jsonStatusBody(res));
};

fetcher.search = (encodedSearchTerm) => {
  return fetch(`${BASE_URL}/products/search/${encodedSearchTerm}`)
    .then((res) => jsonStatusBody(res));
};

fetcher.products = () => {
  return fetch(`${BASE_URL}/products/`)
    .then((res) => jsonStatusBody(res));
};

fetcher.activeProducts = () => {
  return fetch(`${BASE_URL}/products/active`)
    .then((res) => jsonStatusBody(res));
};

fetcher.addProduct = (body) => {
  return fetch(`${BASE_URL}/products`, {
    method: 'POST',
    headers: { 'Authorization': getAuthHeader(), 'Content-Type': 'application/json'},
    body: JSON.stringify(body)
  })
    .then((res) => jsonStatusBody(res));
};

fetcher.updateProduct = (productId, body) => {
  return fetch(`${BASE_URL}/products/${productId}`, {
    method: 'PATCH',
    headers: { 'Authorization': getAuthHeader(), 'Content-Type': 'application/json'},
    body: JSON.stringify(body)
  })
    .then((res) => jsonStatusBody(res));
};

fetcher.addToCart = (productId, quantity) => {
  const body = { productId: productId, quantity: quantity }
  return runFetch('/cart', 'POST', { sendToken: true, body: body});
};


fetcher.addToWishlist = (productId) => {
  return runFetch(`/products/${productId}/wish`, 'POST', { sendToken: true });
}

fetcher.unWish = (productId) => {
  return runFetch(`/products/${productId}/unwish`, 'POST', { sendToken: true });
}

// /orders
fetcher.order = (orderId) => {
  return fetch(`${BASE_URL}/orders/${orderId}`, {
    headers: { Authorization: getAuthHeader() }
  })
  .then((res) => jsonStatusBody(res));
};

fetcher.orders = () => {
  return runFetch('/orders', 'GET', { sendToken: true });
};

fetcher.userOrders = (userId) => {
  return runFetch(`/orders/user/${userId}`, 'GET', { sendToken: true });
};

//fetcher.orders = () => {
  //return fetch(`${BASE_URL}/orders`, {
      //headers: { Authorization: getAuthHeader() }
  //})
    //.then((res) => jsonStatusBody(res));
//};

fetcher.productsFromIdList = async (productIds) => {
      const products = [];
        for (let i = 0; i < productIds.length; i++) {
          products.push(await fetcher.product(productIds[i]).then(({data}) => data));
        }
      return new Promise((p) => p({ data: products }));
}

fetcher.orderProducts = async (orderId) => {
  return fetcher.order(orderId)
    .then(async ({ data }) => {
      const products = [];
      const productIds = data.products.map(op => op.productId);
      return fetcher.productsFromIdList(productIds);
    });
}


fetcher.cart = () => {
  return fetch(`${BASE_URL}/cart`, {
    headers: { Authorization: getAuthHeader() }
  })
  .then((res) => jsonStatusBody(res));
};

fetcher.checkOut = () => {
  return runFetch("/cart/checkout", 'POST', { sendToken: true });
}

fetcher.packed = (orderId) => {
  return fetch(`${BASE_URL}/orders/${orderId}/packed`, {
    method: 'POST',
    headers: { Authorization: getAuthHeader() }
  })
  .then((res) => jsonStatusBody(res));
};

fetcher.shipped = (orderId) => {
  return fetch(`${BASE_URL}/orders/${orderId}/shipped`, {
    method: 'POST',
    headers: { Authorization: getAuthHeader() }
  })
  .then((res) => jsonStatusBody(res));
};

fetcher.delivered = (orderId) => {
  return fetch(`${BASE_URL}/orders/${orderId}/delivered`, {
    method: 'POST',
    headers: { Authorization: getAuthHeader() }
  })
  .then((res) => jsonStatusBody(res));
};

fetcher.refundRequested = (orderId) => {
  return fetch(`${BASE_URL}/orders/${orderId}/refundRequested`, {
    method: 'POST',
    headers: { Authorization: getAuthHeader() }
  })
  .then((res) => jsonStatusBody(res));
};

fetcher.refundApproved = (orderId) => {
  return fetch(`${BASE_URL}/orders/${orderId}/refundApproved`, {
    method: 'POST',
    headers: { Authorization: getAuthHeader() }
  })
  .then((res) => jsonStatusBody(res));
};

fetcher.returned = (orderId) => {
  return fetch(`${BASE_URL}/orders/${orderId}/returned`, {
    method: 'POST',
    headers: { Authorization: getAuthHeader() }
  })
  .then((res) => jsonStatusBody(res));
};

fetcher.rate = (productId, orderId, stars, comment) => {

  const body = {
      stars: stars,
      comment: comment,
      orderId: orderId,
  }

  return runFetch(`/products/${productId}/rate`, 'POST', {
    sendToken: true,
    body: body
  })
}

fetcher.BASE_URL = BASE_URL;
fetcher.FRONTEND_BASE_URL = FRONTEND_BASE_URL;

export default fetcher;
