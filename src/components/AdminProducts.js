import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';

import ProductGallery from './ProductGallery';

export default function AdminProducts() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetcher.products()
      .then(({status, data}) => {
        setProducts(data.reverse());
      })
  }, [])

  return (
    <>
      <Button variant="success" as={Link} to="/admin/products/addProduct">Add new product</Button>
      <ProductGallery products={products} admin={true} />
    </>
  )
};

// vim: ft=javascriptreact
