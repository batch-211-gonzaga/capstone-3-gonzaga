export default function Stars({rating}) {
  let stars = [];
  for (let i = 0; i < Math.floor(rating); i++) {
    const key = stars.length;
    stars.push(<i className="fa-solid fa-star" key={key} />)
  }

  if (rating % 1 > 0.2) {
    const key = stars.length;
    stars.push(<i className="fa-solid fa-star-half-stroke" key={key}></i>)
  }

  while (stars.length < 5) {
    const key = stars.length;
    stars.push(<i className="fa-regular fa-star" key={key}></i>);
  }

  return (
    <span className="star">
    {stars}
    </span>
  )
};
