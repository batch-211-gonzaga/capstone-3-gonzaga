import { InputGroup, Button, Badge, Container, Image } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useContext } from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import UserContext from '../UserContext';
import fetcher from '../fetcher';

export default function HeaderButtons() {
  const { user, wishlistNum, cartNum, updateUserInfo } = useContext(UserContext);

  const navigate = useNavigate();
  const [update, setUpdate] = useState(0);

  function forceUpdate() {
    return () => setUpdate(v => v + 1)
  }

  function directTo(href) {
    navigate(href);
    forceUpdate();
  }

  const logInUrl = `login/${encodeURIComponent(window.location.pathname.slice(1))}`;

                  //<Button variant="secondary" className="mx-2 position-relative" style={{right: "20px"}} as={Link} to={"/account/cart" }>
  return (
    <>
      <div className="d-flex">
        {user ? (
          <>
              {user.isAdmin ?
                <Button variant="danger" className="mx-2" as={Link} to="/admin">Admin</Button>
              :
                <>
                  <Button variant="secondary" className="mx-2 position-relative" style={{right: "20px"}} as={Link} to={"/account/cart"}>
                    <i className="fa-solid fa-cart-shopping"></i>
                    {(cartNum > 0) && <Badge className="mx-2 position-absolute top-0 start-100 translate-middle" bg="danger">
                      {cartNum}
                    </Badge>}
                  </Button>
                  <Button variant="secondary" className="mx-2 position-relative" style={{right: "10px"}} as={Link} to={"/account/wishlist"}>
                    <i className="fa-solid fa-heart"></i>
                  {(wishlistNum > 0) && <Badge className="mx-2 position-absolute top-0 start-100 translate-middle" bg="danger">
                      {wishlistNum}
                    </Badge>}
                  </Button>
                  <Button variant="secondary" className="mx-2" as={Link} to="/account">Account</Button>
                </>
              }
              <Button variant="secondary" className="mx-2" as={Link} to="/logout">
                Log out
              </Button>
          </>
        ) : (
          <>
            <Button variant="secondary" className="mx-2" as={Link} to={logInUrl}>
              Log in
            </Button>
            <Button variant="secondary" className="mx-2" as={Link} to="/register">
              Register
            </Button>
          </>
        )}
      </div>
    </>
  );
}

// vim: ft=javascriptreact
