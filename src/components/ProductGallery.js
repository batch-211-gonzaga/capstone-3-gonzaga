import { useState, useEffect, useRef } from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';
import { useContext } from 'react';
import UserContext from '../UserContext';
import fetcher from '../fetcher';

export default function ProductGallery({products, admin, cart, wishlist}) {
  const [ productsComponent, setProductsComponent ] = useState([]);

  useEffect(() => {
    const pc = products.map(product =>
        <Col sm={6} md={6} lg={4} xl={3} className="my-3" key={product._id}>
          <Card>
            <ProductCard product={product} admin={admin} cart={cart} wishlist={wishlist} />
          </Card>
        </Col>
    );
    setProductsComponent(pc);
  }, [products]);


  let toRender = <></>;
  if (productsComponent.length > 0)
    toRender = productsComponent;
  else
    toRender = <h4>No products to show</h4>

  return (
    <Container className="mb-3">
      <Row>
        { (productsComponent.length > 0) ? productsComponent : <h4>No products to show</h4> }
      </Row>
    </Container>
  )
};

// vim: ft=javascriptreact
