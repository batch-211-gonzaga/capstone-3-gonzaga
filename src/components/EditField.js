import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';
import { Modal } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';

export default function EditField({ value, label, onConfirm }) {
  const [showModal, setShowModal] = useState(false);
  const [inputComponet, setInputComponent] = useState(<></>)
  const [valueComponent, setValueComponent] = useState(<></>)

  useEffect(() => {
    console.log('value:', value);
    if (value === undefined || value === null)
      return;

    setValueComponent(<small>{value}</small>);
    if (typeof value === 'string') {
        setInputComponent(<Form.Control type="textarea" defaultValue={value} />);
        if (value.length > 55)
          setInputComponent(<Form.Control as="textarea" rows={4} defaultValue={value} />);
        if (value.length > 100)
          setInputComponent(<Form.Control as="textarea" rows={10} defaultValue={value} />);
        if (value.length > 150)
          setInputComponent(<Form.Control as="textarea" rows={15} defaultValue={value} />);
    } else if (typeof value === 'number') {
      setInputComponent(<Form.Control type="number" defaultValue={value} />);
    } else if (value === true) {
          setInputComponent(<Form.Check type="switch" defaultChecked={true} />);
          setValueComponent(<small>Yes</small>);
        }
    else if (value === false) {
      setInputComponent(<Form.Check type="switch" defaultChecked={false} />);
      setValueComponent(<small>No</small>);
    }

  }, [value]);

  function openModal() { setShowModal(true); }
  function closeModal() { setShowModal(false); }

  function submit(e) {
    e.preventDefault();

    if (typeof value === 'boolean')
     onConfirm(e.target[0].checked);
    else
     onConfirm(e.target[0].value);

    closeModal()
  }

  return (
    <>
      <span>
        <strong style={{ marginRight: '1rem' }}>
          <Link onClick={openModal} style={{ marginRight: '0.3rem' }}>
            <i className="fa-solid fa-pen-to-square"></i>
          </Link>
          {label}:
        </strong>
        {valueComponent}
      </span>

      <Modal show={showModal}>
        <Modal.Header>
          <Modal.Title>{label}</Modal.Title>
        </Modal.Header>
        <Form onSubmit={submit}>
          <Modal.Body>
            {inputComponet}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={closeModal}>
              Cancel
            </Button>
            <Button
              variant="primary"
              type="submit"
            >
              Save Changes
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

// vim: ft=javascriptreact
