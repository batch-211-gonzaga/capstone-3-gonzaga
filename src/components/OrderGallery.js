import { Container, Row, Col } from 'react-bootstrap';
import OrderCard from './OrderCard';
import AdminOrderCard from './AdminOrderCard';

export default function OrderGallery({ orders, admin }) {
  const orderComponents = orders.map(order =>
    <Col sm={12} key={order._id}>
      {admin ?
        <OrderCard order={order} key={order._id} admin={true} />
      :
        <OrderCard order={order} key={order._id} />
      }
    </Col>
  )

        //<AdminOrderCard order={order} key={order._id} />

  return (
    <>
      <Container>
        <Row>
          {orderComponents}
        </Row>
      </Container>
    </>
  )
};

// vim: ft=javascriptreact

