import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Image } from 'react-bootstrap';
import { Card } from 'react-bootstrap';

import { InputGroup } from 'react-bootstrap';
import { Form } from 'react-bootstrap';

import UserContext from '../UserContext';
import fetcher from '../fetcher';
import ProductGallery from './ProductGallery';

export default function BestSellingProducts({ products }) {

  function compareSold(a, b) {
    if (a.numSold > b.numSold)
      return -1
    else if (a.numSold < b.numSold)
      return 1
    else
      return 0
  }

  function compareRating(a, b) {
    if (a.rating > b.rating)
      return -1
    else if (a.rating < b.rating)
      return 1
    else
      return 0
  }

  function arrangeBySold(products){
    products.sort(compareSold);
    return products.filter(p => (p.numSold > 0)).slice(0, 8)
  }

  function arrangeByRating(products) {
    products.sort(compareRating);
    return products.filter(p => (p.ratings.length > 0 && p.rating > 3)).slice(0, 8)
  }

  const productsBySold = arrangeBySold(products);
  const productsByRating = arrangeByRating(products);

  return (
    <div className="justify-content-center">
      <h2>Best selling</h2>
      <ProductGallery products={productsBySold} />
      <h2>Highest rated</h2>
      <ProductGallery products={productsByRating} />
    </div>
  )
};

// vim: ft=javascriptreact
